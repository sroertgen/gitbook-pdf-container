FROM node:slim

RUN npm install --global gitbook-cli &&\
	gitbook fetch &&\
	apt-get update -y && \
	apt-get install git -y &&\
	apt-get install calibre -y &&\
	rm -rf /tmp/*


CMD [""]
